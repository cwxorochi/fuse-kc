public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_TextControl_string_Value_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.TextControl _obj;
        public Fuse_Controls_TextControl_string_Value_Property(Fuse.Controls.TextControl obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Value; }
        protected override void OnSet(string v, object origin) { _obj.SetValue(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged -= listener; }
    }
    public sealed class Fuse_Controls_Toggle_Fuse_Triggers_IValue_bool__Target_Property: Uno.UX.Property<Fuse.Triggers.IValue<bool>>
    {
        Fuse.Controls.Toggle _obj;
        public Fuse_Controls_Toggle_Fuse_Triggers_IValue_bool__Target_Property(Fuse.Controls.Toggle obj) { _obj = obj; }
        protected override Fuse.Triggers.IValue<bool> OnGet() { return _obj.Target; }
        protected override void OnSet(Fuse.Triggers.IValue<bool> v, object origin) { _obj.Target = v; }
    }
    MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
    MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
    MainView.Fuse_Controls_Toggle_Fuse_Triggers_IValue_bool__Target_Property temp2_Target_inst;
    internal Fuse.Controls.Panel loggedInView;
    internal Fuse.Controls.Panel homePage;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Controls.TextInput();
        temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
        var temp1 = new Fuse.Controls.TextInput();
        temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
        var temp2 = new Fuse.Controls.Toggle();
        temp2_Target_inst = new MainView.Fuse_Controls_Toggle_Fuse_Triggers_IValue_bool__Target_Property(temp2);
        var temp3 = new Fuse.Controls.Panel();
        var temp4 = new Fuse.iOS.StatusBarConfig();
        loggedInView = new Fuse.Controls.Panel();
        homePage = new Fuse.Controls.Panel();
        var temp5 = new Fuse.Reactive.JavaScript();
        var temp6 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "username");
        var temp7 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "password");
        var temp8 = new Fuse.Controls.Button();
        var temp9 = new Fuse.Gestures.Clicked();
        var temp10 = new Fuse.Reactive.DataBinding<Fuse.Triggers.IValue<bool>>(temp2_Target_inst, "login");
        this.ClearColor = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
        temp3.Children.Add(loggedInView);
        temp3.Children.Add(homePage);
        temp3.Behaviors.Add(temp4);
        temp4.IsVisible = true;
        temp4.Style = Uno.Platform.iOS.StatusBarStyle.Light;
        loggedInView.Name = "loggedInView";
        loggedInView.IsEnabled = false;
        homePage.Name = "homePage";
        homePage.Children.Add(temp);
        homePage.Children.Add(temp1);
        homePage.Children.Add(temp8);
        homePage.Behaviors.Add(temp5);
        temp5.Code = "var Observable = require(\"FuseJS/Observable\");\n\n\t\t\t\tvar username = Observable(\"\");\n\t\t\t\tvar password = Observable(\"\");\n\n\t\t\t\tvar logged = Observable(false);\n\n\t\t\t\tfunction login()\n\t\t\t\t{\n\t\t\t\t\tif(username.value != \"\" && password.value != \"\")\n\t\t\t\t\t\tlogged.value = true;\n\t\t\t\t\telse\n\t\t\t\t\t\tlogged.value = false;\n\t\t\t\t}\n\n\t\t\t\tmodules.exports = {\n\t\t\t\t\tusername : username, \n\t\t\t\t\tpassword : password,\n\t\t\t\t\tlogged : logged,\n\t\t\t\t\tlogin : login\n\t\t\t\t};";
        temp5.LineNumber = 10;
        temp5.FileName = "MainView.ux";
        temp.PlaceholderText = "Username";
        temp.Behaviors.Add(temp6);
        temp1.IsPassword = true;
        temp1.PlaceholderText = "Password";
        temp1.Behaviors.Add(temp7);
        temp8.Behaviors.Add(temp10);
        temp8.Behaviors.Add(temp9);
        temp9.Actions.Add(temp2);
        this.RootNode = temp3;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
    }
}
