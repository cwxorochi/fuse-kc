public partial class MyRectangle: Fuse.Controls.Rectangle
{
    static MyRectangle()
    {
    }
    public MyRectangle()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        this.MinWidth = 100f;
        this.MinHeight = 200f;
    }
}
public partial class MainView: Fuse.App
{
    internal Fuse.iOS.StatusBarConfig statusBarConfig;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Controls.DockPanel();
        statusBarConfig = new Fuse.iOS.StatusBarConfig();
        this.ClearColor = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
        temp.Behaviors.Add(statusBarConfig);
        statusBarConfig.IsVisible = true;
        statusBarConfig.Style = Uno.Platform.iOS.StatusBarStyle.Light;
        this.RootNode = temp;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
    }
}
