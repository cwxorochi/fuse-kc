public partial class MainView: Fuse.App
{
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Controls.StackPanel();
        var temp1 = new Fuse.Controls.TextInput();
        var temp2 = new Fuse.Controls.TextInput();
        var temp3 = new Fuse.Controls.Button();
        this.ClearColor = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp.Margin = float4(40f, 0f, 40f, 0f);
        temp.Children.Add(temp1);
        temp.Children.Add(temp2);
        temp.Children.Add(temp3);
        temp1.PlaceholderText = "Username";
        temp2.IsPassword = true;
        temp3.Text = "Submit";
        temp3.Alignment = Fuse.Elements.Alignment.Center;
        temp3.Margin = float4(0f, 50f, 0f, 0f);
        temp3.Padding = float4(40f, 20f, 40f, 20f);
        this.RootNode = temp;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
    }
}
